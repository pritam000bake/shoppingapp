﻿using Microsoft.EntityFrameworkCore;
using ShoppingAPI.Model;

namespace ShoppingAPI.Context
{
    public class BookingDbContext:DbContext
    {
        public BookingDbContext()
        {

        }
        public BookingDbContext(DbContextOptions<BookingDbContext> Context) : base(Context)
        {

        }
        public DbSet<Booking> Cart { get; set; }
    }
}
