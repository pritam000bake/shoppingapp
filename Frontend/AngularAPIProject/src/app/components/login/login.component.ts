import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from 'src/app/models/login-user';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  constructor(private userService: UserService , private router: Router) {
    console.log("Inside log in method");
    
  }

  ngOnInit(): void {

  }
  login(loginUser: LoginUser) {
    console.log(loginUser);
    if(loginUser.name=="pritam"&&loginUser.password=="pritam"){
      console.log("Admin login Sucess")
      Swal.fire(
        'Admin Login',
        'Admin Login success',
        'success'
      )
      this.router.navigateByUrl("/admindashboard")
    }
    else{
      this.userService.login(loginUser).subscribe(res => {
        console.log(res);
        let jsonObject = JSON.stringify(res);
        let jsonToken = JSON.parse(jsonObject) 
        console.log('User Token After Login :::: ${jsonToken["Token]}');
        localStorage.setItem('userToken', jsonToken["Token"]);
        
          Swal.fire(
            'User Login',
            'User Login Success',
            'success'
          )
          this.router.navigateByUrl(`/dashboard/${loginUser.name}`)
      })

    }
  }
}
